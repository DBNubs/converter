const fs = require('fs');
const path = require('path');
const fetch = require('isomorphic-fetch');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const urls = [
    {nid: '115146', title: 'Speaking of climate', path: 'http://greenbiz.lndo.site/newsletter-30/speaking-climate', date: '2020-02-24 10:00:00'},
    {nid: '115106', title: 'I’m a big fan of wind logistics ', path: 'http://greenbiz.lndo.site/newsletter-30/im-big-fan-wind-logistics', date: '2020-02-20 23:01:00'},
    {nid: '115107', title: 'Four ways to scale regenerative ag', path: 'http://greenbiz.lndo.site/newsletter-30/four-ways-scale-regenerative-ag', date: '2020-02-20 18:25:00'},
    {nid: '115105', title: 'The rising tide of city resilience', path: 'http://greenbiz.lndo.site/newsletter-30/rising-tide-city-resilience', date: '2020-02-19 14:00:00'},
    {nid: '115104', title: 'Utilities need to aggressively embrace EVs', path: 'http://greenbiz.lndo.site/newsletter-30/utilities-need-aggressively-embrace-evs', date: '2020-02-18 14:00:00'},
    {nid: '115103', title: 'Gone fishin', path: 'http://greenbiz.lndo.site/newsletter-30/gone-fishin', date: '2020-02-17 10:00:00'},
    {nid: '115031', title: 'The right track ', path: 'http://greenbiz.lndo.site/newsletter-30/right-track', date: '2020-02-14 23:01:00'},
    {nid: '115108', title: "Composting's complexities", path: 'http://greenbiz.lndo.site/newsletter-30/compostings-complexities', date: '2020-02-21 23:01:00'},
    {nid: '115028', title: 'The secret to a happy relationship', path: 'http://greenbiz.lndo.site/newsletter-30/secret-happy-relationship', date: '2020-02-13 23:01:00'},
    {nid: '115030', title: 'Introducing Food Weekly', path: 'http://greenbiz.lndo.site/newsletter-30/introducing-food-weekly', date: '2020-02-13 18:25:00'},
    {nid: '115091', title: 'TESTING Ads', path: 'http://greenbiz.lndo.site/newsletter-30/testing-ads', date: '2020-02-10 10:00:00'},
    {nid: '115027', title: 'Gadget, power thyself', path: 'http://greenbiz.lndo.site/newsletter-30/gadget-power-thyself', date: '2020-02-12 23:01:00'},
    {nid: '115026', title: 'The breakout biofuel for heavy-duty fleets', path: 'http://greenbiz.lndo.site/newsletter-30/breakout-biofuel-heavy-duty-fleets', date: '2020-02-11 14:00:00'},
    {nid: '115025', title: 'Phoenix rising', path: 'http://greenbiz.lndo.site/newsletter-30/phoenix-rising', date: '2020-02-10 10:00:00'},
    {nid: '115024', title: 'It depends', path: 'http://greenbiz.lndo.site/newsletter-30/it-depends', date: '2020-02-07 23:01:00'},
    {nid: '115021', title: 'VERGE Energy just got more powerful ', path: 'http://greenbiz.lndo.site/newsletter-30/verge-energy-just-got-more-powerful', date: '2020-02-06 23:01:00'},
    {nid: '115020', title: 'VERGE Weekly: Today’s special', path: 'http://greenbiz.lndo.site/newsletter-30/verge-weekly-todays-special', date: '2020-02-05 14:00:00'},
    {nid: '115018', title: "Hello 'climate tech'", path: 'http://greenbiz.lndo.site/newsletter-30/hello-climate-tech', date: '2020-02-04 14:00:00'},
    {nid: '115017', title: 'Introducing … VERGE Food', path: 'http://greenbiz.lndo.site/newsletter-30/introducing-verge-food', date: '2020-02-03 10:00:00'},
    {nid: '114971', title: "Plastic bottles' clothes encounter ", path: 'http://greenbiz.lndo.site/newsletter-30/plastic-bottles-clothes-encounter', date: '2020-01-31 23:01:00'},
    {nid: '114970', title: 'Greetings from DistribuTech! ', path: 'http://greenbiz.lndo.site/newsletter-30/greetings-distributech', date: '2020-01-30 23:01:00'},
    {nid: '114969', title: 'Follow the patents', path: 'http://greenbiz.lndo.site/newsletter-30/follow-patents', date: '2020-01-29 23:01:00'},
    {nid: '114965', title: 'Davos, without snark', path: 'http://greenbiz.lndo.site/newsletter-30/davos-without-snark', date: '2020-01-27 10:00:00'},
    {nid: '114968', title: 'GM & Cruise rev up the service model', path: 'http://greenbiz.lndo.site/newsletter-30/gm-cruise-rev-service-model', date: '2020-01-28 14:00:00'},
    {nid: '114904', title: 'From bad to worse', path: 'http://greenbiz.lndo.site/newsletter-30/bad-worse', date: '2020-01-24 23:01:00'},
    {nid: '114903', title: 'New York is kicking California’s butt in building electrification incentives', path: 'http://greenbiz.lndo.site/newsletter-30/new-york-kicking-californias-butt-building-electrification-incentives', date: '2020-01-23 23:01:00'},
    {nid: '114902', title: 'Positively climate-positive ', path: 'http://greenbiz.lndo.site/newsletter-30/positively-climate-positive', date: '2020-01-22 14:00:00'},
    {nid: '114899', title: 'Making a material difference', path: 'http://greenbiz.lndo.site/newsletter-30/making-material-difference', date: '2020-01-20 10:00:00'},
    {nid: '114848', title: 'Metrics of the trade', path: 'http://greenbiz.lndo.site/newsletter-30/metrics-trade', date: '2020-01-17 23:01:00'},
    {nid: '114847', title: 'Don’t say Amazon isn’t doing anything', path: 'http://greenbiz.lndo.site/newsletter-30/dont-say-amazon-isnt-doing-anything', date: '2020-01-16 23:01:00'},
    {nid: '114900', title: 'We need electric ride-hailing', path: 'http://greenbiz.lndo.site/newsletter-30/we-need-electric-ride-hailing', date: '2020-01-21 14:00:00'},
    {nid: '114846', title: 'Raining fire: It’s time for a tech reckoning', path: 'http://greenbiz.lndo.site/newsletter-30/raining-fire-its-time-tech-reckoning', date: '2020-01-15 14:00:00'},
    {nid: '114844', title: 'The State of Green Business, 2020', path: 'http://greenbiz.lndo.site/newsletter-30/state-green-business-2020', date: '2020-01-13 10:00:00'},
    {nid: '114811', title: 'The PFAS and the furious ', path: 'http://greenbiz.lndo.site/newsletter-30/pfas-and-furious', date: '2020-01-10 23:01:00'},
    {nid: '114845', title: "Tech's private commuter buses need batteries", path: 'http://greenbiz.lndo.site/newsletter-30/techs-private-commuter-buses-need-batteries', date: '2020-01-14 14:00:00'},
    {nid: '114808', title: 'Climate clamor at CES', path: 'http://greenbiz.lndo.site/newsletter-30/climate-clamor-ces', date: '2020-01-08 23:01:00'},
    {nid: '114805', title: 'The year ahead', path: 'http://greenbiz.lndo.site/newsletter-30/year-ahead', date: '2020-01-06 10:00:00'},
    {nid: '114809', title: 'What energy resilience could look like', path: 'http://greenbiz.lndo.site/newsletter-30/what-energy-resilience-could-look', date: '2020-01-09 23:01:00'},
    {nid: '114806', title: '2020 will be a key year (and decade) for electric vehicles', path: 'http://greenbiz.lndo.site/newsletter-30/2020-will-be-key-year-and-decade-electric-vehicles', date: '2020-01-07 14:00:00'},
    {nid: '114785', title: '2019 was the year that…', path: 'http://greenbiz.lndo.site/newsletter-30/2019-was-year', date: '2019-12-30 10:00:00'},
    {nid: '114786', title: 'Welcome to the AI decade', path: 'http://greenbiz.lndo.site/newsletter-30/welcome-ai-decade', date: '2020-01-02 23:01:00'},
    {nid: '114709', title: 'Closing the loop on 2019', path: 'http://greenbiz.lndo.site/newsletter-30/closing-loop-2019', date: '2019-12-20 23:01:00'},
    {nid: '114686', title: 'Clean energy megatrends of the 2010s', path: 'http://greenbiz.lndo.site/newsletter-30/clean-energy-megatrends-2010s', date: '2019-12-19 23:01:00'},
    {nid: '114707', title: 'VERGE Weekly: Time for a rhyme', path: 'http://greenbiz.lndo.site/newsletter-30/verge-weekly-time-rhyme', date: '2019-12-18 14:00:00'},
    {nid: '114705', title: 'Bad COP', path: 'http://greenbiz.lndo.site/newsletter-30/bad-cop', date: '2019-12-16 10:00:00'},
    {nid: '114708', title: 'The moral hazard of celebrating incrementalism', path: 'http://greenbiz.lndo.site/newsletter-30/moral-hazard-celebrating-incrementalism', date: '2019-12-12 23:01:00'},
    {nid: '114683', title: "It's AI to the rescue", path: 'http://greenbiz.lndo.site/newsletter-30/its-ai-rescue', date: '2019-12-11 23:01:00'},
    {nid: '114706', title: 'This is state-led transportation leadership', path: 'http://greenbiz.lndo.site/newsletter-30/state-led-transportation-leadership', date: '2019-12-17 14:00:00'},
    {nid: '114682', title: 'Over a barrel', path: 'http://greenbiz.lndo.site/newsletter-30/over-barrel', date: '2019-12-09 10:00:00'},
    {nid: '114638', title: 'Consuming our way to a circular economy? ', path: 'http://greenbiz.lndo.site/newsletter-30/consuming-our-way-circular-economy', date: '2019-12-06 23:01:00'},
    {nid: '114637', title: 'Home Depot’s circle in the box store ', path: 'http://greenbiz.lndo.site/newsletter-30/home-depots-circle-box-store', date: '2019-12-05 23:01:00'},
    {nid: '114636', title: 'VERGE Weekly: COP25 - Because the Ocean', path: 'http://greenbiz.lndo.site/newsletter-30/verge-weekly-cop25-because-ocean', date: '2019-12-04 23:01:00'},
    {nid: '114634', title: 'The new language of weather', path: 'http://greenbiz.lndo.site/newsletter-30/new-language-weather', date: '2019-12-02 10:00:00'},
    {nid: '114635', title: 'The mobility tech trends you should be following', path: 'http://greenbiz.lndo.site/newsletter-30/mobility-tech-trends-you-should-be-following', date: '2019-12-10 14:00:00'},
    {nid: '114610', title: 'The profession of sustainability', path: 'http://greenbiz.lndo.site/newsletter-30/profession-sustainability', date: '2019-11-25 10:00:00'},
    {nid: '114567', title: 'Imitation game', path: 'http://greenbiz.lndo.site/newsletter-30/imitation-game', date: '2019-11-22 23:01:00'},
    {nid: '114566', title: 'Rethinking microgrids', path: 'http://greenbiz.lndo.site/newsletter-30/rethinking-microgrids', date: '2019-11-21 23:01:00'},
    {nid: '114565', title: 'SAP’s simple, sensible strategy', path: 'http://greenbiz.lndo.site/newsletter-30/saps-simple-sensible-strategy', date: '2019-11-20 23:01:00'},
    {nid: '114562', title: 'ESG, OMG', path: 'http://greenbiz.lndo.site/newsletter-30/esg-omg', date: '2019-11-18 10:00:00'},
    {nid: '114499', title: 'Glare Tactics', path: 'http://greenbiz.lndo.site/newsletter-30/glare-tactics', date: '2019-11-15 18:00:00'},
    {nid: '114496', title: 'Inside McDonald’s new renewables deal ', path: 'http://greenbiz.lndo.site/newsletter-30/inside-mcdonalds-new-renewables-deal', date: '2019-11-14 23:01:00'},
    {nid: '114564', title: 'Electric school buses to crack open vehicle-to-grid', path: 'http://greenbiz.lndo.site/newsletter-30/electric-school-buses-crack-open-vehicle-grid', date: '2019-11-19 14:00:00'},
    {nid: '114487', title: 'VERGE Weekly: Telling the truth', path: 'http://greenbiz.lndo.site/newsletter-30/verge-weekly-telling-truth', date: '2019-11-13 14:00:00'},
    {nid: '114485', title: 'A resilient food revolution', path: 'http://greenbiz.lndo.site/newsletter-30/resilient-food-revolution', date: '2019-11-11 10:00:00'},
    {nid: '114442', title: 'A watt saved is a watt earned ', path: 'http://greenbiz.lndo.site/newsletter-30/watt-saved-watt-earned', date: '2019-11-07 23:01:00'},
    {nid: '114441', title: 'This AI idea has many fans', path: 'http://greenbiz.lndo.site/newsletter-30/ai-idea-has-many-fans', date: '2019-11-06 23:01:00'},
    {nid: '114486', title: 'The great EV infrastructure challenge', path: 'http://greenbiz.lndo.site/newsletter-30/great-ev-infrastructure-challenge', date: '2019-11-12 14:00:00'},
    {nid: '114439', title: 'Circular firing squad?', path: 'http://greenbiz.lndo.site/newsletter-30/circular-firing-squad', date: '2019-11-04 10:00:00'},
    {nid: '114372', title: 'Reclaiming power in California', path: 'http://greenbiz.lndo.site/newsletter-30/reclaiming-power-california', date: '2019-10-31 22:01:00'},
    {nid: '114440', title: 'Get out to vote, if you care about clean, electric transportation', path: 'http://greenbiz.lndo.site/newsletter-30/get-out-vote-if-you-care-about-clean-electric-transportation', date: '2019-11-05 14:00:00'},
    {nid: '114416', title: 'Burning questions', path: 'http://greenbiz.lndo.site/newsletter-30/burning-questions', date: '2019-10-28 09:00:00'},
    {nid: '114367', title: 'Wish you were here!', path: 'http://greenbiz.lndo.site/newsletter-30/wish-you-were-here', date: '2019-10-24 22:01:00'},
    {nid: '114366', title: 'Hooked on blockchain', path: 'http://greenbiz.lndo.site/newsletter-30/hooked-blockchain', date: '2019-10-23 22:01:00'},
    {nid: '114403', title: 'Sink or swim', path: 'http://greenbiz.lndo.site/newsletter-30/sink-or-swim', date: '2019-10-21 09:00:00'},
    {nid: '114324', title: 'Gather? Why bother? ', path: 'http://greenbiz.lndo.site/newsletter-30/gather-why-bother', date: '2019-10-18 22:01:00'},
    {nid: '114323', title: 'Will you be there?', path: 'http://greenbiz.lndo.site/newsletter-30/will-you-be-there', date: '2019-10-17 22:01:00'},
    {nid: '114371', title: 'The word from VERGE', path: 'http://greenbiz.lndo.site/newsletter-30/word-verge', date: '2019-10-30 13:00:00'},
    {nid: '114370', title: 'A conversation with California clean air warrior Mary Nichols', path: 'http://greenbiz.lndo.site/newsletter-30/conversation-california-clean-air-warrior-mary-nichols', date: '2019-10-29 13:00:00'},
    {nid: '114320', title: 'From ambition to action', path: 'http://greenbiz.lndo.site/newsletter-30/ambition-action', date: '2019-10-16 13:00:00'},
    {nid: '114318', title: 'Is this anything?', path: 'http://greenbiz.lndo.site/newsletter-30/anything', date: '2019-10-14 09:00:00'},
    {nid: '114253', title: 'A climate advocate walks into an oil industry summit… ', path: 'http://greenbiz.lndo.site/newsletter-30/climate-advocate-walks-oil-industry-summit', date: '2019-10-10 22:01:00'},
    {nid: '114252', title: 'AI in action', path: 'http://greenbiz.lndo.site/newsletter-30/ai-action', date: '2019-10-09 22:01:00'},
    {nid: '114250', title: 'Circularity and the 45% climate solution', path: 'http://greenbiz.lndo.site/newsletter-30/circularity-and-45-climate-solution', date: '2019-10-07 09:00:00'},
    {nid: '114174', title: ' How to cut campus food waste ', path: 'http://greenbiz.lndo.site/newsletter-30/how-cut-campus-food-waste', date: '2019-10-04 22:01:00'},
    {nid: '114254', title: 'Products from thin air', path: 'http://greenbiz.lndo.site/newsletter-30/products-thin-air', date: '2019-10-11 22:01:00'},
    {nid: '114251', title: 'Workshops Test', path: 'http://greenbiz.lndo.site/newsletter-30/workshops-test', date: '2019-10-08 13:00:00'},
    {nid: '114173', title: 'Merry Energy Efficiency Day!', path: 'http://greenbiz.lndo.site/newsletter-30/merry-energy-efficiency-day', date: '2019-10-03 22:01:00'},
    {nid: '114169', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business-6', date: '2019-09-30 09:00:00'},
    {nid: '114172', title: 'Investing in the magic machine', path: 'http://greenbiz.lndo.site/newsletter-30/investing-magic-machine', date: '2019-10-02 13:00:00'},
    {nid: '114170', title: "What it'll take for cargo ships to go zero emission ", path: 'http://greenbiz.lndo.site/newsletter-30/what-itll-take-cargo-ships-go-zero-emission', date: '2019-10-01 13:00:00'},
    {nid: '114072', title: 'Big companies, big commitments', path: 'http://greenbiz.lndo.site/newsletter-30/big-companies-big-commitments', date: '2019-09-26 22:01:00'},
    {nid: '114069', title: 'Disrupting Unilever', path: 'http://greenbiz.lndo.site/newsletter-30/disrupting-unilever', date: '2019-09-25 22:01:00'},
    {nid: '114066', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business-5', date: '2019-09-23 09:00:00'},
    {nid: '114067', title: "What you should know about Amazon's huge EV deal", path: 'http://greenbiz.lndo.site/newsletter-30/what-you-should-know-about-amazons-huge-ev-deal', date: '2019-09-24 13:00:00'},
    {nid: '113987', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business-4', date: '2019-09-16 09:00:00'},
    {nid: '113969', title: 'How to work with your utility', path: 'http://greenbiz.lndo.site/newsletter-30/how-work-your-utility', date: '2019-09-12 22:01:00'},
    {nid: '113968', title: 'The hunger for food entrepreneurs', path: 'http://greenbiz.lndo.site/newsletter-30/hunger-food-entrepreneurs', date: '2019-09-11 22:01:00'},
    {nid: '113966', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business-3', date: '2019-09-09 21:00:00'},
    {nid: '113967', title: 'Why city fleets are leading on green vehicles ', path: 'http://greenbiz.lndo.site/newsletter-30/why-city-fleets-are-leading-green-vehicles', date: '2019-09-10 13:00:00'},
    {nid: '113837', title: 'Power Points', path: 'http://greenbiz.lndo.site/newsletter-30/power-points-1', date: '2019-08-29 22:01:00'},
    {nid: '113836', title: 'Blockchain, the missing link for reporting?', path: 'http://greenbiz.lndo.site/newsletter-30/blockchain-missing-link-reporting', date: '2019-08-28 22:01:00'},
    {nid: '113834', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business-2', date: '2019-08-26 21:00:00'},
    {nid: '113835', title: 'Scooters need to embrace the circular economy', path: 'http://greenbiz.lndo.site/newsletter-30/scooters-need-embrace-circular-economy', date: '2019-08-27 13:00:00'},
    {nid: '113786', title: 'Can blockchain catalyze carbon removal?', path: 'http://greenbiz.lndo.site/newsletter-30/can-blockchain-catalyze-carbon-removal', date: '2019-08-21 22:01:00'},
    {nid: '113787', title: 'Power Points', path: 'http://greenbiz.lndo.site/newsletter-30/power-points-0', date: '2019-08-22 22:01:00'},
    {nid: '113785', title: 'Why batteries need AI', path: 'http://greenbiz.lndo.site/newsletter-30/why-batteries-need-ai', date: '2019-08-20 13:00:00'},
    {nid: '113739', title: 'Power Points', path: 'http://greenbiz.lndo.site/newsletter-30/power-points', date: '2019-08-15 22:01:00'},
    {nid: '113735', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business-0', date: '2019-08-12 21:00:00'},
    {nid: '113737', title: 'Decarbonizing fleets starts with these 3 tactics', path: 'http://greenbiz.lndo.site/newsletter-30/decarbonizing-fleets-starts-these-3-tactics', date: '2019-08-13 13:00:00'},
    {nid: '112192', title: 'Taking Care of Business', path: 'http://greenbiz.lndo.site/newsletter-30/taking-care-business', date: '2019-04-29 21:00:00'}
];

// for(var i = 0; i < urls.length; i++) {
    
//     currentObj = urls[i];
//     console.log(currentObj['title']);
    
//     fetch(urls[i]['path'])
//         .then(res => {
//             // Create File
//             var url = res.url;
//             var urlPath = url.replace("http://greenbiz.lndo.site/newsletter-30/", '');
//             urlPath = urlPath.substring(urlPath.lastIndexOf("/") + 1);

//             /**
//              * GETS HTML PAGES AND WRITES TO FILES
//              */
//             // if (res.status == 502) {
//             //     var filepath = path.resolve(__dirname, 'scraped/errors/' + urlPath + ".html"); 
//             // } else {
//             //     var filepath = path.resolve(__dirname, 'scraped/pages/' + urlPath + ".html"); 
//             // }
            
//             // if (!fs.existsSync(filepath)) {
//             //     // Write file
//             //     const dest = fs.createWriteStream(filepath);
//             //     res.body.pipe(dest);
//             // }
            
//         })
//         .catch((err) => {
//             console.log(err);
//         })
// }

// Create Writer for csv file
const csvWriter = createCsvWriter({
    path: __dirname + '/scraped/csv/newsl.csv',
    header: [
        {id: 'nid', title: 'nid'},
        {id: 'title', title: 'title'},
        {id: 'langcode', title: 'langcode'},
        {id: 'field_newsl_date_send', title: 'field_newsl_date_send'},
        {id: 'body', title: 'body'}
    ]
});

var records = [];

for(var i = 0; i < urls.length; i++) {
    
    var currentObj = urls[i];

    var url = currentObj['path'];
    var urlPath = url.replace("http://greenbiz.lndo.site/newsletter-30/", '');
    urlPath = urlPath.substring(urlPath.lastIndexOf("/") + 1);

    /**
     * READS HTML FILES AND WRITES TO CSV
     */
    var filepath = path.resolve(__dirname, 'scraped/pages/' + urlPath + ".html"); 

    var data = fs.readFileSync(filepath, 'utf8');
    
    var record = {
            nid: currentObj['nid'], 
            title: currentObj['title'], 
            langcode: 'en',
            field_newsl_date_send: currentObj['date'],
            body: data.toString()
    };

    records.push(record);
    
}

csvWriter.writeRecords(records)
        .then(() => {
            console.log(' ...Done');
        }); 